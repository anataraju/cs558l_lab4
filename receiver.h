/*receiver.h*/
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include "list.h"

#define FILE_NAME_LENGTH 256
#define MAX_PATH_LENGTH 1024

#define RECEIVER_TCP_CONTROL_PORT    5001       // file name sent, ACKs received
#define RECEIVER_UDP_DATA_PORT   6001           // transfer 

#define PACKET_SIZE 32768 

//Receiver states
#define INITIAL_STATE 0
#define CONT_RECVD 1
#define CONT_ACK_SENT 2
#define EOF_RCVD 4
#define END_OF_TRANSFER 8

#define ASSERT(x) if(!(x)) { fprintf( stderr,  "%s (%d): Assertion failed!!!\n", __FILE__, __LINE__); }
#define IS_SET(s, b) ((s & b) == b)

#define DEBUG 1

#define DEBUG_ERR(msg) {fprintf(stderr, "%s (%d): %s\n", __FILE__, __LINE__, msg);  exit(1);}
#ifdef DEBUG
	#define DEBUG_MSG(msg) {fprintf(stderr, "%s (%d): %s\n", __FILE__, __LINE__, msg);}
#else
	#define DEBUG_MSG(msg) {}//do nothing)
#endif 



typedef struct session
{
    struct stat _stat;                  /*file stats*/
    FILE * fp;             
    int tcp_listen_sockfd;
	int tcp_sockfd;
    int    udp_sockfd;                  
    char  local_file_path[FILE_NAME_LENGTH]; /*populated from the prg input*/
    char  remote_file_path[FILE_NAME_LENGTH]; 
    size_t packet_size;                 /*size of each packet*/
  /*  unsigned seq_num;                   
    unsigned last_ack; */
    unsigned int conn_state; 
	char *dest_path;
	char  dest_dir[MAX_PATH_LENGTH];       // where the file needs to be placed at the receiver
    	char  file_name[FILE_NAME_LENGTH];      // name of the file being transferred
    	unsigned int file_size;                 // size
    	long long num_packets;

	long long expected_seqno;

} session_t;

/* control packet to be recvd over TCP*/
typedef struct control
{
    char  dest_dir[MAX_PATH_LENGTH];       // where the file needs to be placed at the receiver
    char  file_name[FILE_NAME_LENGTH];      // name of the file being transferred
    long long file_size;                 // size
    long long num_packets;               // todo: number of packets that make up the file
} control_t;

/* packet header */
typedef struct header
{
    int end_of_file;
    unsigned long long pkt_id;
    unsigned int pkt_size;      /*in bytes*/
    /*unsigned int ack_no;*/        /*ack sent by the receiver*/
} header_t;

/* data packet to be sent over UDP*/
typedef struct packet
{
	header_t msg_header;
	unsigned char pkt_data[PACKET_SIZE];    
} packet_t;


session_t sd; /* session data */


// we receive ack on TCP
typedef struct ack_packet
{
    long long ack_no;
} ack_packet_t;


/* Create a list of packets */
List p_list;

/*
1. check file permissions (validate) - use stat
2. 
*/
FILE * open_file (char *file_name);

/*we should be able to seek and read particular chunk of file, beginning at a byte offset*/
size_t read_data (FILE * fp, size_t bytes_to_read);

/*inputs: RTT
output: optimal packet size*/
size_t calc_packet_size(); // Venkat

/*procedure:
calculate bandwidth delay product*/
size_t calc_num_packets_to_send();

/*Uses TCP connection to receive fie path and other info*/
int recv_control();

/*Uses TCP connection to receive ACKs [both for control and data]*/
int recv_ack();

/*Uses UDP to send the packet_data*/
int send_data();

/*Uses UDP to receive the packet_data*/
int recv_data();

/* Open TCP conn */
int TCPconn();

/* Open UDP conn */
int UDPconn();

/* Send an ACK for the data */
int send_ack(long long);

int write_to_file(FILE *fp, packet_t *pkt);
void resetConnState();
FILE* open_file(char* filename);
int recv_UDP(packet_t *p);

int packet_deepcopy(packet_t *dst, packet_t *src);

void packet_destroy(packet_t *pkt);


