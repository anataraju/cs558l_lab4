/*server.h*/
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <pthread.h>

#define FILE_NAME_LENGTH            256
#define MAX_PATH_LENGTH             1024
#define DEST_NAME_LENGTH            256
#define MAX_HOSTNAME_LENGTH			256

#define RECEIVER_TCP_CONTROL_PORT   5001       // file name sent, ACKs received
#define RECEIVER_UDP_DATA_PORT      6001       // transfer 
#define MY_UDP_PORT		    7001

#define TCP_SEND_BUFFER             1024

#define PACKET_SIZE                 32768 
#define TEST_DELAY_USECS            1000

#define DEBUG 1
//#define DEBUG_1 1

typedef struct session
{
    int id;
    struct stat _stat;                  /*file stats*/
    FILE * fp;             
    int    tcp_sockfd;
    int    udp_sockfd;                  
    size_t packet_size;                 /*size of each packet*/
    long long seq_num;                      // Current Sequence number
    long long retransmit_seq_num;         // Sequence number to retrasmit 
    long long last_ack;
    unsigned char endOfFile;
    char local_file_path[MAX_PATH_LENGTH];
    struct sockaddr_in udpSendTo; 
    struct sockaddr_in myAddr; 
    char  dest_name[MAX_HOSTNAME_LENGTH];	// destination hostname
    long long num_packets;
} session_t;

typedef struct control
{
    char  dest_dir[MAX_PATH_LENGTH];		// where the file needs to be placed at the receiver
    char  file_name[FILE_NAME_LENGTH];      // name of the file being transferred
    //char  dest_name[MAX_HOSTNAME_LENGTH];	// destination hostname
    long long file_size;					// size
    long long num_packets;					// todo: number of packets that make up the file
} control_t;

typedef struct header { 
  int end_of_file;
  long long pkt_id;
  unsigned int pkt_size;
} header_t;

/* data packet to be sent over UDP*/
typedef struct packet
{
    header_t msg_header;
    unsigned char pkt_data[PACKET_SIZE];    
          /*in bytes*/
} packet_t;



// wrapper for the tcp_controller thread input arg
typedef struct sess_ctl
{
	session_t *p_session;
	control_t *p_control;
} sess_ctl_t;

// we receive ack on TCP
typedef struct ack_packet
{
    long long ack_no;        // carries the nak
} ack_packet_t;

pthread_mutex_t mtx_ack_check;
unsigned int nak_recvd;
unsigned int end_xmit;

pthread_mutex_t mtx_thread_ctl;
pthread_cond_t	cv_send;// variable to signal udp thread to start sending the data
unsigned int recvd_start_signal = 0;
/*
1. check file permissions (validate) - use stat
2. 
*/
FILE * open_file (char *file_name);

/*we should be able to seek and read particular chunk of file, beginning at a byte offset*/
size_t read_data (FILE * fp, size_t bytes_to_send);

/*inputs: RTT
output: optimal packet size*/
size_t calc_packet_size(); // Venkat

/*procedure:
calculate bandwidth delay product*/
size_t calc_num_packets_to_send();

/* Used for opening UDP connection and populating
socket descriptor in session */
int udpClient ( session_t *localSession);
