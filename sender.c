/*sender main file*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <errno.h>
#include <time.h>
#include <sys/time.h>

#include "sender.h"

void make_packet(session_t *,packet_t *, int );
void send_data(session_t* , packet_t*);

int packet_deepcopy(packet_t *dst, packet_t *src)
{
    if(src == NULL)
        return -1;

    memcpy((unsigned char *)&dst->msg_header, (unsigned char *)&src->msg_header, sizeof(header_t));
    memcpy((unsigned char *)dst->pkt_data, (unsigned char *)src->pkt_data, PACKET_SIZE);    

    return 0;
}

// thread function 1: tcp sender and ack receiver
void * tcp_controller(void * arg)
{
    int sockfd, n;
    struct sockaddr_in serv_addr;
    struct hostent *server;
    size_t c_len = sizeof(control_t);
    unsigned char send_buffer[c_len];
	unsigned char *recv_buffer = NULL;
	ack_packet_t *p_ack_pkt;
    int i = 0;

#ifdef DEBUG
    fprintf (stdout, "In TCP Controller: Thread started\n");
#endif
    sess_ctl_t *pwrap = (sess_ctl_t *) arg;

	control_t *pctl = pwrap->p_control;
	session_t *pses = pwrap->p_session;

    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) { 
        fprintf (stderr, "tcp controller: ERROR opening tcp socket\n");
		return NULL;
	}

	// todo: set sockopt - keep alive

    server = gethostbyname(pses->dest_name);
    if (server == NULL) {
        fprintf(stderr,"tcp controller: ERROR no such destination host\n");
        return (NULL);
    }
    
    memset((char *) &serv_addr,0, sizeof(serv_addr));

    serv_addr.sin_family = AF_INET;
    bcopy((char *)server->h_addr, (char *)&serv_addr.sin_addr.s_addr, server->h_length);
    bcopy((char *)server->h_addr, (char *)&pses->udpSendTo.sin_addr.s_addr, server->h_length);
  
    serv_addr.sin_port = htons(RECEIVER_TCP_CONTROL_PORT);
    
    if (connect(sockfd, (struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0)  {
        fprintf(stderr, "tcp controller: ERROR connecting to destination host\n");
		return NULL;
	}

#ifdef DEBUG
	//fprintf(stdout, "%s (%d): My TCP port No-->%d, Remote host name->%s, Remote Port no-->%d", __FILE__, __LINE__,
	
#endif
	// send the control information
	memset(send_buffer,0, c_len);
	/* Done: Populate the control packet contents */
	memcpy(send_buffer, pctl, c_len);

    n = write(sockfd, send_buffer, sizeof(send_buffer));
    if (n < 0) {
       fprintf(stderr, "tcp_controller: ERROR writing to socket");
	   return NULL;
	}

#ifdef DEBUG
    fprintf (stdout, "In TCP Controller: Waiting for first ACK\n");
#endif
	recv_buffer = (unsigned char *)malloc(sizeof(ack_packet_t));
    while (1) 
    // for (i = 0; i < 1; ++i)
    {
        memset(recv_buffer, 0, sizeof(ack_packet_t));

		n = read(sockfd, recv_buffer, sizeof(ack_packet_t));	// flags = 0, behaves like write
		if (n < 0) {
             fprintf (stderr, "tcp controller: ERROR reading from socket\n");
			 return NULL;
		}
		
		p_ack_pkt = (ack_packet_t *) recv_buffer;
		if (p_ack_pkt->ack_no == -1) {

#ifdef DEBUG            
			fprintf (stdout, "tcp controller: recvd ack for control packet\n");
            fprintf (stdout, "In TCP Controller: Signaling the UDP sender to start\n");
#endif

			pthread_mutex_lock(&mtx_thread_ctl);
			recvd_start_signal = 1;

			pthread_cond_signal (&cv_send);
			pthread_mutex_unlock(&mtx_thread_ctl);
		} else {
			
            pthread_mutex_lock(&mtx_ack_check);
			if (p_ack_pkt->ack_no  >= pses->num_packets) {
				end_xmit = 1;
				pthread_mutex_unlock(&mtx_ack_check);
#ifdef DEBUG
                fprintf (stdout, "In TCP Controller: recvd last ACK from recv\n");
                fprintf (stdout, "In TCP Controller: UDP sender notified to stop\n");
#endif
				break;

			} else {
				nak_recvd = 1;
				/* UPDATED: re-transmit sequence no */
				pses->retransmit_seq_num = p_ack_pkt->ack_no;
#ifdef DEBUG_1
                fprintf (stdout, "In TCP Controller: received NAK for seq_no:%lld\n", p_ack_pkt->ack_no);
#endif                

			}
			pthread_mutex_unlock(&mtx_ack_check);
		}
        
        // fprintf(stdout, "tcp controller: server says:%s",buffer);
    }

	free(recv_buffer);
	close(sockfd);
	return NULL;
}

int initialize(session_t *psession, control_t * pcntl, const char *file_name, const char *rem_host, const char *rem_dir)
{
    int ret = 0;

#ifdef DEBUG
    fprintf(stdout, "Entered initialize\n");
#endif    
    pthread_mutex_init(&mtx_ack_check, NULL);
	pthread_mutex_init(&mtx_thread_ctl, NULL);
	pthread_cond_init (&cv_send, NULL);

    memset(psession, 0, sizeof (session_t));
    memset(pcntl, 0, sizeof(control_t));

    // set the session paramters
    ret = stat(file_name, &psession->_stat);
    if (ret < 0) {
        fprintf(stderr, "stat call failed: error");
        return 0;
    }
    strncpy(psession->local_file_path, file_name, MAX_PATH_LENGTH);

    // set the control packet
    strncpy(pcntl->file_name, file_name, FILE_NAME_LENGTH);
    strncpy(pcntl->dest_dir, rem_dir, MAX_PATH_LENGTH);
    strncpy(psession->dest_name, rem_host, DEST_NAME_LENGTH);
    pcntl->file_size = (long long) (psession->_stat.st_size);
	
    pcntl->num_packets = (long long) (psession->_stat.st_size / PACKET_SIZE);

    	if (psession->_stat.st_size % PACKET_SIZE > 0)
		pcntl->num_packets += 1;
    psession->num_packets = pcntl->num_packets;	
    // i have my doubts
#ifdef DEBUG
    fprintf(stdout, "File name: %s\n", pcntl->file_name);
    fprintf(stdout, "Dest dir: %s\n", pcntl->dest_dir);
    fprintf(stdout, "Dest Host name: %s\n", psession->dest_name);
    fprintf(stdout, "File size: %lld\n", pcntl->file_size);
    fprintf(stdout, "Calculated number of packets: %lld\n", psession->num_packets);
#endif

    return ret;
}

void* udp_sender( void* arg) { 
  
    // open File and update the file pointer in the session_t
    
    FILE *fp;
    int i = 0;
	/* Amogh: begin */
	/* We need to wait until the first ACK has been received to ensure control info has reached the receiver */

#ifdef DEBUG
    fprintf (stdout, "In UDP Sender: Thread started\n");
#endif

	sess_ctl_t *pwrap = (sess_ctl_t *) arg;

	// control_t *pctl = pwrap->p_control; // not used 
	session_t *pses = pwrap->p_session;

	pthread_mutex_lock(&mtx_thread_ctl);
	while (recvd_start_signal == 0) {
		pthread_cond_wait(&cv_send, &mtx_thread_ctl);
	}
	pthread_mutex_unlock(&mtx_thread_ctl);
	/* Amogh: end */

	if (recvd_start_signal) {
    	// start sending the data
    	
    	fp = open_file(pses->local_file_path);
    	pses->fp = fp;	
     
	/* UPDATED: Adding UDPClient for UDP connection */
	(void) udpClient(pses);

    	// We need to get the packet , We will make make_packet,  
    	packet_t *packetToSend = NULL;
    	packetToSend = (packet_t * ) malloc( sizeof(packet_t)); 

        packet_t *lastPacket=NULL;
        lastPacket = (packet_t *) malloc(sizeof(packet_t));
        //lastPacket->pkt_data = (unsigned char *) malloc(sizeof(unsigned char) * PACKET_SIZE);

    	int retransmission = 0;
        
        //for (i = 0; i < 2; ++i) {
    	while (1) { 

                //memset (lastPacket,0, sizeof(packet_t));
    		if (pses->endOfFile !=  1) {
                       memset( lastPacket,0,sizeof(packet_t));		
                       make_packet(pses,packetToSend,retransmission);
                
                if (packet_deepcopy(lastPacket, packetToSend) < 0) {
                    fprintf(stderr, "UDP sender: Packet deep copy failed\n");
                }

    			send_data(pses,packetToSend);
    		} else { 
#ifdef DEBUG                
                fprintf(stdout, "udp sender: just sending the last packet\n" );
#endif
                send_data(pses,lastPacket);
            }

    		pthread_mutex_lock(&mtx_ack_check); 
    		
    		if(nak_recvd) {
    			nak_recvd=0;
    			pthread_mutex_unlock(&mtx_ack_check);
    			retransmission = 1;
    			make_packet(pses,packetToSend,retransmission);
#ifdef DEBUG
                fprintf (stdout, "udp sender: re-transmitting packet: %lld\n",
                        packetToSend->msg_header.pkt_id);
#endif
    			send_data(pses,packetToSend);
    			retransmission = 0;
    		} else { 
    			pthread_mutex_unlock(&mtx_ack_check);
    		}
          
    		if (end_xmit) {
#ifdef DEBUG
    			fprintf (stdout, "udp sender: ending transmission\n");
#endif
    			close(pses->udp_sockfd);
    			fclose(pses->fp);
    			free(packetToSend);
    			break;
    		}
    	}
    }
    return NULL;	
}

void make_packet(session_t* s, packet_t* p, int retransmit) 
{ 
  
	int nbytes;
    FILE *fp;
    if (NULL != s)
        fp = s->fp;
    
    if (NULL == fp) {
        fprintf(stderr, "udp sender: cannot open file - null pointer\n");
        // error
        exit(0);
    }

  
	//p->pkt_data = (unsigned char *)malloc(PACKET_SIZE*sizeof(char));
  
	if (retransmit) { 
#ifdef DEBUG
        fprintf(stdout, "Entered retransmit section\n");
#endif		

        if (fseek(fp,s->retransmit_seq_num * PACKET_SIZE, SEEK_SET) < 0  )  { 
           fprintf(stderr, "fseek returned error\n");
            exit(0);
        }
      
        memset (p->pkt_data,0, PACKET_SIZE);
		nbytes=fread(p->pkt_data,1,PACKET_SIZE,fp); 
		if ( nbytes < 0 ) { 
			fprintf(stderr,"fread returned error\n");
			exit(0);
		} 
		p->msg_header.pkt_size = nbytes;
		p->msg_header.pkt_id=s->retransmit_seq_num;
		if ( (fseek(fp,s->seq_num * PACKET_SIZE,SEEK_SET)) < 0 ) {
            fprintf(stderr, "fseek returned error\n");
            exit(0);
        }
        /* TO DO : changed to s->nm_packet-1 */
        if (s->retransmit_seq_num == s->num_packets-1) {
            p->msg_header.end_of_file = 1;
            s->endOfFile = 1;
        } else {
            p->msg_header.end_of_file = 0;
            // s->endOfFile = 0;
        }
  
		/*if ( nbytes > 0 && nbytes < PACKET_SIZE ) { 
			// Doing Padding here
			p->msg_header.end_of_file = 1;
			s->endOfFile=1;
		} else { 
			p->msg_header.end_of_file = 0;
		}*/
	} else { 
      
		if ( (fseek(fp,s->seq_num * PACKET_SIZE, SEEK_SET)) < 0 ) {
            fprintf(stderr, "fseek returned error\n" );
            exit(0);
        }

        memset (p->pkt_data,0, PACKET_SIZE);

		nbytes=fread(p->pkt_data,1,PACKET_SIZE,fp); 
		if ( nbytes < 0 ) { 
			fprintf(stderr,"fread returned error\n");
			exit(0);
		}     
#ifdef DEBUG
        fprintf(stdout, "udp sender: read %d bytes of actual data\n", nbytes);
        fprintf(stdout, "udp sender: TTTT %lld seq num being sent\n", s->seq_num);
#endif

		p->msg_header.pkt_size = nbytes;
		p->msg_header.pkt_id=s->seq_num;
		//s->seq_num += 1;

        if (s->seq_num == s->num_packets-1) {
            p->msg_header.end_of_file = 1;
            s->endOfFile = 1;
        } else {
            p->msg_header.end_of_file = 0;
            s->endOfFile = 0;
        }
        s->seq_num+=1;
   
/*		if ( nbytes > 0 && nbytes < PACKET_SIZE ) { 
			// Doing Padding here
#ifdef DEBUG
        fprintf(stdout, "udp sender: padding and setting end of file\n");
#endif            
			int bytesToPad = PACKET_SIZE-nbytes;
			bzero(p->pkt_data+nbytes,bytesToPad);
			
		}  else { 
			p->msg_header.end_of_file = 0;
		}*/
	}

}

void send_data(session_t* s,packet_t* p) 
{ 
	int retcode;
#ifdef DEBUG
    fprintf(stdout, "Sending Packet: %lld\n end of file: %d\n", p->msg_header.pkt_id,
                p->msg_header.end_of_file);
    fprintf(stdout, "packet size: %u\n", p->msg_header.pkt_size);

#endif
	retcode = sendto(s->udp_sockfd,(void*)p,PACKET_SIZE+sizeof(header_t),0,(struct sockaddr *) &s->udpSendTo,
 			sizeof(s->udpSendTo));
	if (retcode <= -1) {
		printf("client: sendto failed: %d\n",errno); 
		exit(0);  
	}

//    usleep(TEST_DELAY_USECS);  
}


FILE* open_file(char* filename) 
{ 
    
	FILE *fp=NULL;
	fp = fopen(filename,"rb");
	if ( fp == NULL ) { 
		fprintf(stderr,"fopen returned error\n");
		exit(0);
	}

#ifdef DEBUG
    fprintf(stdout, "File :%s opened successfully for sending\n", filename);
#endif
	return fp;
}

/*switch to getopt or getoptlong*/
int main(int argc, char **argv)
{
    int err = 0;
	sess_ctl_t sctl;
	pthread_t tcp_th_id, udp_th_id;
	// pthread_attr_t attr;
	void *res;	// pthread_join result - don't care

    struct timeval start, end;
    double throughput = 0.0f;
    long timediff = 0;

    session_t ft_session;
    control_t ft_control;

    if (argc < 3) {
        fprintf(stderr, "Need 3 arguments atleast\n");
        fprintf(stderr, "sender <local_file> <remote_host> <remote_dir>\n\n");
        return 0;
    }
    // fill up the ft_session
	

    err = initialize(&ft_session, &ft_control, argv[1], argv[2], argv[3]);
    if (err < 0) {
		fprintf(stderr, "could not initialize\n");
		return 0;
    }

	sctl.p_session = &ft_session;
	sctl.p_control = &ft_control;

    gettimeofday(&start, NULL);

	err = pthread_create (&tcp_th_id, NULL, &tcp_controller, &sctl);
	if (err < 0) {
		fprintf (stderr, "error in pthread_create tcp_controller\n");
		return 0;
	}
		
	err = pthread_create (&udp_th_id, NULL, &udp_sender, &sctl);
	if (err < 0) {
		fprintf (stderr, "error in pthread_create udp_sender\n");
		return 0;
	}

	/*
	err = pthread_attr_destroy(&attr);
	if (err < 0) {
		fprintf (stderr, "error in pthread_attr_destroy\n");
		return 0;
	}
	*/

	err = pthread_join(tcp_th_id, &res);
	if (err < 0) {
		fprintf (stderr, "error in pthread_join tcp_controller\n");
		return 0;
	}

	err = pthread_join(udp_th_id, &res);
	if (err < 0) {
		fprintf (stderr, "error in pthread_join udp_sender\n");
		return 0;
	}

    gettimeofday(&end, NULL);

    fprintf(stdout, "Timediff: %ld(us)\n", ((end.tv_sec * 1000000 + end.tv_usec)
          - (start.tv_sec * 1000000 + start.tv_usec)));
    
    timediff = ((end.tv_sec * 1000000 + end.tv_usec) - (start.tv_sec * 1000000 + start.tv_usec));

    throughput = ( (double) (ft_control.file_size) * 8.0) / ((double)(timediff) * (1.024 * 1.024));
    fprintf(stdout, "Throughput: %lf\n", throughput);

	return err;
}

int udpClient ( session_t *localSession)  { 
		
	int sockfd; 
#ifdef DEBUG    
	fprintf (stdout, "Entered udpClient: Creating UDP socket\n" );
#endif
	// Creating the socket 
	if ( (sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0)  {
		fprintf(stderr,"Server :socket error: %d\n",errno);
	    exit(0); 
	}
	
	// Setting the Struct to Zero , Don't use Memset , it doesn't work 
	memset((char *) &localSession->myAddr, 0, sizeof(localSession->myAddr)); 
	localSession->myAddr.sin_family= AF_INET;
	localSession->myAddr.sin_addr.s_addr=htons(INADDR_ANY);
	localSession->myAddr.sin_port = htons(MY_UDP_PORT);

	// Now Bind this Addr
    if ( (bind(sockfd, (struct sockaddr *) &localSession->myAddr, sizeof(localSession->myAddr)) < 0) ) { 
    	fprintf(stderr,"Server: bind fail: %d\n",errno); 
    	exit(0); 
    }

    localSession->udpSendTo.sin_family=AF_INET;
    localSession->udpSendTo.sin_port=htons(RECEIVER_UDP_DATA_PORT);
    localSession->udp_sockfd= sockfd;
    
    return sockfd;
}

