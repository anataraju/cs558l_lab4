/*
 * Author:      Ankit JAin (jain698@usc.edu)
 *
 * Code used from CS 402 class
 */

#ifndef _LIST_H_
#define _LIST_H_

#ifndef NULL
#define NULL 0L
#endif /* ~NULL */

#ifndef TRUE
#define FALSE 0
#define TRUE 1
#endif /* ~TRUE */

#ifdef WIN32
#define DIR_SEP '\\'
#else /* ~WIN32 */
#define DIR_SEP '/'
#endif /* WIN32 */

#ifndef max
#define max(A,B) (((A)>(B)) ? (A) : (B))
#define min(A,B) (((A)>(B)) ? (B) : (A))
#endif /* ~max */

#ifndef round
#define round(X) (((X) >= 0) ? (int)((X)+0.5) : (int)((X)-0.5))
#endif /* ~round */

#ifndef MAXPATHLENGTH
#define MAXPATHLENGTH 256
#endif /* ~MAXPATHLENGTH */

typedef struct tagListElem {
	void *obj;
    	struct tagListElem *next;
    	struct tagListElem *prev;
} ListElem;

typedef struct tagList {
    int num_members;
    ListElem anchor;

    /* You do not have to set these function pointers */
    int  (*Length)(struct tagList *);
    int  (*Empty)(struct tagList *);

    int  (*Append)(struct tagList *, void*);
    int  (*Prepend)(struct tagList *, void*);
    void (*Unlink)(struct tagList *, ListElem*);
    void (*UnlinkAll)(struct tagList *);
    int  (*InsertBefore)(struct tagList *, void*, ListElem*);
    int  (*InsertAfter)(struct tagList *, void*, ListElem*);

    ListElem *(*First)(struct tagList *);
    ListElem *(*Last)(struct tagList *);
    ListElem *(*Next)(struct tagList *, ListElem *cur);
    ListElem *(*Prev)(struct tagList *, ListElem *cur);

    ListElem *(*Find)(struct tagList *, void *obj);
   	int *(*ListExistsById)(struct tagList *, long long  id);
	int *(*InsertIntoList)(struct tagList *, void *element);
	int *(*CompareTransElem)(void *src, void *dst);
} List;

extern int  ListLength(List*);
extern int  ListEmpty(List*);

extern int  ListAppend(List*, void*);
extern int  ListPrepend(List*, void*);
extern void ListUnlink(List*, ListElem*);
extern void ListUnlinkAll(List*);
extern int  ListInsertAfter(List*, void*, ListElem*);
extern int  ListInsertBefore(List*, void*, ListElem*);

extern ListElem *ListFirst(List*);
extern ListElem *ListLast(List*);
extern ListElem *ListNext(List*, ListElem*);
extern ListElem *ListPrev(List*, ListElem*);

extern ListElem *ListFind(List*, void*);

extern int ListInit(List*);
int ListExistsById(List* list, long long id);
int InsertIntoList(List *list, void *element);
int CompareTransElem(void *src, void *dst);

#endif /*_LIST_H_*/
