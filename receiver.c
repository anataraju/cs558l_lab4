#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include "receiver.h"
#include <arpa/inet.h>

void error(const char *msg)
{

    	perror(msg);
    	exit(1);
}

/*
void DEBUG_ERR(const char *msg)
{
    fprintf(stderr, "%s (%d): %s\n", __FILE__, __LINE__, msg);
    exit(1);
}

void DEBUG_MSG(const char *msg)
{
#ifdef DEBUG
    fprintf(stdout, "%s (%d): %s\n", __FILE__, __LINE__, msg);
#endif
}
*/

int main(int argc, char** argv)
{

	DEBUG_MSG("In Main");

	//Check command line arguments
	//fetch port?

	while(1)
	{

		//Clear the session variable
		memset((unsigned char *) &sd, 0, sizeof (session_t));
	
		//check connection state
		//Open and bind a TCP socket
		if(TCPconn() < 0)
		{
			DEBUG_ERR("Error opening TCP socket");
		}

		//block for data read
		(void ) recv_control();

		//Send ack
		(void) send_ack(-1);

		//change state
		sd.conn_state = CONT_ACK_SENT;

		//Open and bind UDP socket
		if(UDPconn() < 0)
		{
			DEBUG_ERR("Error opening UDP socket");
		}

		//Initialize the List of packets
		ListInit(&p_list);
		sd.expected_seqno = 0;
		
		char *filename = (char *) malloc (sizeof(char) * (MAX_PATH_LENGTH + FILE_NAME_LENGTH));
		sprintf(filename, "%s/%s",  sd.dest_dir, sd.file_name);

		//Open file to write
		sd.fp = open_file(filename);
		/* TODO: Need to return error if file opening failed. 
			The error needs to be communicated to sender */

		//Receive message
		while(!IS_SET(sd.conn_state, EOF_RCVD))
		{
			recv_data();
		} 

		//done, 
		sd.conn_state = END_OF_TRANSFER;

		//Clear all state from receiver
		resetConnState();
	}
}

//Open and bind a TCP socket
int TCPconn()
{
	DEBUG_MSG("In TCPConn");

	int sockfd, childsockfd;
     	struct sockaddr_in serv_addr, cli_addr;
     	int opt=1 ;
    	socklen_t clilen;

	//open socket
	sockfd = socket(AF_INET, SOCK_STREAM, 0);
     	if (sockfd < 0) 
        	error("ERROR opening socket");

	
     	memset((unsigned char *) &serv_addr,0, sizeof(serv_addr));
     	serv_addr.sin_family = AF_INET;
     	serv_addr.sin_addr.s_addr = INADDR_ANY;
     	serv_addr.sin_port = htons(RECEIVER_TCP_CONTROL_PORT);

     	//set socket options to reuse a port
     	if( setsockopt(sockfd,SOL_SOCKET,SO_REUSEADDR,(const char *)&opt,sizeof(int)) < 0)
		error("ERROR on setsockopt");
	
	//
     	if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) 
		error("ERROR on binding");

#ifdef DEBUG
    	fprintf(stdout, "%s (%d): Opened TCP socket on port %d\n", __FILE__, __LINE__, RECEIVER_TCP_CONTROL_PORT);
#endif

	// Populate the TCP listen socket
	sd.tcp_listen_sockfd = sockfd;
	
	//Listen
	listen(sockfd,5);

	//Accept
	clilen = sizeof(cli_addr);
     	childsockfd = accept(sockfd, 
               		(struct sockaddr *) &cli_addr, 
                 &clilen);

     	if (childsockfd < 0) 
          	error("ERROR on accept");

#ifdef DEBUG
    	fprintf(stdout, "%s (%d): Got a coonection request. Opened TCP child socket %d\n", __FILE__, __LINE__, childsockfd);
#endif

	// Populate the TCP child socket
	sd.tcp_sockfd = childsockfd;

	return 0;
}

//Open and bind a UDP socket
int UDPconn()
{

	DEBUG_MSG("In UDPconn");
	int sockfd;
     	struct sockaddr_in serv_addr;
     	int opt=1;
    	
	//open socket
	sockfd = socket(AF_INET, SOCK_DGRAM, 0);
     	if (sockfd < 0) 
        	error("ERROR opening socket");

	
     	memset((unsigned char *) &serv_addr,0, sizeof(serv_addr));
     	serv_addr.sin_family = AF_INET;
     	serv_addr.sin_addr.s_addr = INADDR_ANY;
     	serv_addr.sin_port = htons(RECEIVER_UDP_DATA_PORT);

     	//set socket options to reuse a port
     	if( setsockopt(sockfd,SOL_SOCKET,SO_REUSEADDR,(const char *)&opt,sizeof(int)) < 0)
		error("ERROR on setsockopt");

     	if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) 
		error("ERROR on binding");

#ifdef DEBUG
    	fprintf(stdout, "%s (%d): Opened UDP socket on port %d\n", __FILE__, __LINE__, RECEIVER_UDP_DATA_PORT);
#endif

	// Populate the UDP socket
	sd.udp_sockfd = sockfd;

	return 0;
}

int recv_control()
{	
	DEBUG_MSG("In recv_control");

	int n, p_len = sizeof(control_t);
	control_t *p_control = NULL;	

	unsigned char *buffer = (unsigned char *) malloc(p_len);
	memset(buffer,0, p_len);

	//blocking read
    	n = read(sd.tcp_sockfd,buffer,p_len);
    
	if (n < 0) 
         error("ERROR reading from socket");

	if (n < p_len)
	 DEBUG_ERR("Could not receive proper control packet");

	p_control = (control_t *) buffer;

	/* Populate the path and file name */
	strcpy(sd.dest_dir, p_control->dest_dir);
	strcpy(sd.file_name, p_control->file_name);
	sd.file_size = p_control->file_size;                 // size
    	sd.num_packets = p_control->num_packets;
   
#ifdef DEBUG
    	fprintf(stdout, "%s (%d): Received control packet (%s, %s, %u, %lld) \n", __FILE__, __LINE__, sd.dest_dir, sd.file_name, sd.file_size, sd.num_packets);
#endif

	/* change connection state */
	sd.conn_state = CONT_RECVD;

	free(buffer);

    return 0;
}

/* send an ack with ack_no */
int send_ack(long long ack_no)
{
	DEBUG_MSG("In send_ack");

	int n = 0;

	ack_packet_t msg;
	msg.ack_no = ack_no; 


	n = write(sd.tcp_sockfd, (unsigned char *) &msg, sizeof(msg));
	
	if (n < 0) 
		error("ERROR writing to socket");

#ifdef DEBUG
    	fprintf(stdout, "%s (%d): Sent Acknowledgement with ack_no %lld\n", __FILE__, __LINE__, ack_no);
#endif

	return 0;	

}

/* receive data and act on it */
int recv_data()
{

	DEBUG_MSG("In recv_data");

	packet_t p;
	packet_t *p_cpy;
	header_t *hdr = NULL;

#ifdef DEBUG
    	fprintf(stdout, "%s (%d): Invoking recv_UDP() to get next packet\n", __FILE__, __LINE__);
#endif
	//Receive the packet
	recv_UDP(&p);	



	hdr = (header_t *) &p.msg_header;

#ifdef DEBUG
    	fprintf(stdout, "%s (%d): Expected Seq no is %lld. Received packet with seq no %lld\n", __FILE__, __LINE__, sd.expected_seqno, hdr->pkt_id);
#endif

	//get sequence no
	//1. Duplicate Packet received, already written to file
	// Drop it
	if(hdr->pkt_id < sd.expected_seqno)
	{
#ifdef DEBUG
    	fprintf(stdout, "%s (%d): Received duplicate packet for already acknowledged. Dropped\n", __FILE__, __LINE__);
#endif

		/* TODO: something else ? */
        /*
            Send NAK for next packet expected
            **** only if it is not the last packet 
        */
        if(!(sd.expected_seqno > sd.num_packets) )
        {   
#ifdef DEBUG
        fprintf(stdout, "%s (%d): Sending  NAK (%lld) for last unacknowledged packet.\n", __FILE__, __LINE__, sd.expected_seqno);

#endif
        send_ack(sd.expected_seqno);
        }

		return 0;
	}
	
	/* 2. Received sequenced or out-of-order packet, buffer it
		if not already in buffer */

	else if(hdr->pkt_id >= sd.expected_seqno)
	{
		/* check if data already in buffer */
		if(!ListExistsById(&p_list, hdr->pkt_id))
		{

#ifdef DEBUG
    			fprintf(stdout, "%s (%d): The Packet is not there in buffer. adding it.\n", __FILE__, __LINE__);
#endif

			p_cpy = (packet_t *) malloc(sizeof(packet_t));

			/* TODO: Need to do a Deep copy here */
			packet_deepcopy(p_cpy, &p);
			
			//Insert it into buffer
			InsertIntoList(&p_list, (void *) p_cpy);	

			//Write the buffer to file if expected ack received
			ListElem *elem = ListFirst(&p_list);
			ListElem *nextelem = NULL;

			header_t *h = (header_t *) &(((packet_t *) elem->obj)->msg_header);
	
			while(h->pkt_id == sd.expected_seqno)
			{

#ifdef DEBUG
    			fprintf(stdout, "%s (%d): The Packet is inorder packet. Writing packet id %lld, size %u to file.", __FILE__, __LINE__, h->pkt_id, h->pkt_size);
                fprintf(stdout, ", end of file: %d\n", h->end_of_file);
#endif
				//write the packet to file
				write_to_file(sd.fp, (packet_t *) elem->obj);

				//update expected_seqno
				//sd.expected_seqno += h->pkt_size;
				sd.expected_seqno++;

				nextelem = ListNext(&p_list, elem);
				
				//remove from buffer
				

#ifdef DEBUG
    			fprintf(stdout, "%s (%d): Removing packet id %lld from buffer.\n", __FILE__, __LINE__, h->pkt_id);
                fprintf(stdout, ", end of file: %d\n", h->end_of_file);
#endif
				/* Check whether it was the last packet */
				if(h->end_of_file)
				{
					//Special case, send ack to indicate end of transfer
					send_ack(sd.expected_seqno);

					sd.conn_state = EOF_RCVD;
#ifdef DEBUG
    					fprintf(stdout, "%s (%d): Last packet!!! Setting connection state to End of Transfer (%d) .\n", __FILE__, __LINE__, EOF_RCVD);

#endif
				}

                packet_destroy((packet_t *) elem->obj);
                ListUnlink(&p_list, elem);

				if(nextelem == NULL) break;
				elem = nextelem;
				h = (header_t *) &(((packet_t *) elem->obj)->msg_header);
				
			}

			
		}

        //Send a NAK for out of order packet
        //Send NAK for the last unacknowledged packet in buffer
            if(!ListEmpty(&p_list))
            {

#ifdef DEBUG
                    fprintf(stdout, "%s (%d): Sending  NAK %lld for last unacknowledged packet.\n", __FILE__, __LINE__, sd.expected_seqno);

#endif
                send_ack(sd.expected_seqno);
            }
	}

	return 0;

}

/* receive a UDP packet */
int recv_UDP(packet_t *p)
{
	DEBUG_MSG("In recv_UDP");

	int byte_count;
	socklen_t fromlen;
	struct sockaddr_storage addr;
	int p_len = PACKET_SIZE + sizeof(header_t);

	unsigned char buf[p_len];
	//char ipstr[INET6_ADDRSTRLEN];

	//just recvfrom():

	fromlen = sizeof addr;
	byte_count = recvfrom(sd.udp_sockfd, buf, sizeof buf, 0, ( struct sockaddr *) &addr, &fromlen);

	if(byte_count < p_len)
		DEBUG_ERR("Improper UDP packet received");
	
	// memcpy((char *)p, buf, p_len);
    packet_deepcopy(p, (packet_t *)buf);
	
	//Fetch the Packet header
	
#ifdef DEBUG
    	fprintf(stdout, "%s (%d): recv_UDP() - Received %d bytes\n", __FILE__, __LINE__, byte_count);
#endif

	/*printf("recv()'d %d bytes of data in buf\n", byte_count);*/
	/*printf("from IP address %s\n",
	inet_ntop(addr.ss_family,
	(addr.ss_family == AF_INET)?((struct sockaddr_in *)&addr)->sin_addr:((struct sockaddr_in6 *)&addr)->sin6_addr,
		ipstr, sizeof ipstr);
	*/

	return 0;


}

FILE* open_file(char* filename) {

  FILE *fp=NULL;
  fp=fopen(filename,"wb+");
  if ( fp == NULL ) {
      fprintf(stderr,"fopen returned error while trying to open a File\n");
      exit(0);
  }

#ifdef DEBUG
    	fprintf(stdout, "%s (%d): Opened file %s for writing\n", __FILE__, __LINE__, filename);
#endif

  return fp;
}

/* write data into file
	1. open file if not already open
	2. Write to file
*/
int write_to_file(FILE *fp, packet_t *pkt)
{
	int nbytes;
	header_t *h = (header_t *) &pkt->msg_header;

#ifdef DEBUG
    	fprintf(stdout, "%s (%d): Writing %u bytes to file\n", __FILE__, __LINE__, h->pkt_size);
#endif

   	nbytes=fwrite((unsigned char *) pkt->pkt_data, 1, h->pkt_size, fp);
   	if ( nbytes < 0 )
		DEBUG_ERR("Fwrite returned an error, while writting to file");


	if ( nbytes < h->pkt_size )
		DEBUG_ERR("Fwrite returned an error, complete data not written to file.");

	return nbytes;
}

/* Resets all connection state
	1. Clear all field in session data
	2. Close any open sockets
	3. Any other?
*/
void resetConnState()
{
	/* TODO: To be implemented */
	
	//close sockets
	close(sd.tcp_listen_sockfd);
	close(sd.tcp_sockfd);
    	close(sd.udp_sockfd);

	//close file 
	fclose(sd.fp);

	memset(&sd, 0, sizeof(sd)); 

	//The list should be empty
	ASSERT(ListEmpty(&p_list));
	
}

int packet_deepcopy(packet_t *dst, packet_t *src)
{
	if(src == NULL)
		return -1;

	memcpy((unsigned char *)&dst->msg_header, (unsigned char *)&src->msg_header, sizeof(header_t));
	memcpy((unsigned char *) dst->pkt_data, (unsigned char *) src->pkt_data, PACKET_SIZE);	

	return 0;
}

void packet_destroy(packet_t *pkt)
{
	//Free the packet data
	free(pkt);
}



