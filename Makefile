all: sender receiver

sender: sender.o
	gcc -pthread -o sender sender.o

sender.o: sender.c sender.h
	gcc -g -c -Wall sender.c

receiver: receiver.o list.o
	gcc -o receiver -g receiver.o list.o

list.o: list.c list.h receiver.h
	gcc -g -c -Wall list.c

receiver.o: receiver.c receiver.h list.h
	gcc -g -c -Wall receiver.c

clean:
	rm -f receiver
	rm -f sender
	rm -f *.o 
