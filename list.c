/*
 * Author:      Ankit JAin (jain698@usc.edu)
 *
 * Code used from CS 402 class
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/time.h>
#include "list.h"
#include "receiver.h"

/* ----------------------- Utility Functions ----------------------- */

int ListInit(List* list)
{
	//ListElem *elem = (ListElem *) malloc ( sizeof(ListElem));
	//elem.prev = elem.next = &elem;
	//elem.obj = NULL;
	if (list == NULL) return 0;

	list->anchor.next = list->anchor.prev = &(list->anchor);
	list->anchor.obj = NULL;
	list->num_members = 0;
	return 1;
	
}

int  ListLength(List* list)
{
	if(list == NULL) return -1;
	return list->num_members;
}

int  ListEmpty(List* list)
{
	if(list == NULL || ListLength(list) <=0 ) 
		return TRUE;
	return FALSE;
}

ListElem *ListFirst(List* list)
{
	if ((list == NULL) || (list->anchor.next == &list->anchor)) return NULL;
	return list->anchor.next;
}

ListElem *ListLast(List* list)
{
	if ((list == NULL) || (list->anchor.prev == &list->anchor)) return NULL;
	return list->anchor.prev;
}

ListElem *ListNext(List* list, ListElem* elem)
{
	if((list == NULL) || (elem == NULL) || (elem->next == &list->anchor)) return NULL;
	return elem->next;
}

ListElem *ListPrev(List* list, ListElem* elem)
{
	if((list == NULL) || (elem == NULL) || (elem->prev == &list->anchor)) return NULL;
	return elem->prev;
}


int  ListAppend(List* list, void* data)
{
	if(list == NULL) return FALSE;
	ListElem *elem = (ListElem *) malloc (sizeof(ListElem));
	if(elem == NULL) 
	{
		fprintf(stderr, "Oops! Ran out of memory!!! Exiting..");
		exit(1);	
	}
	elem->obj = data;
	
	elem->prev = list->anchor.prev;		
	list->anchor.prev->next = elem;
	elem->next = &(list->anchor);
	list->anchor.prev = elem;
	
	list->num_members++;		
	return TRUE;
}

int  ListPrepend(List* list, void* data)
{
	if(list == NULL) return FALSE;
	ListElem *elem = (ListElem *) malloc (sizeof(ListElem));
	if(elem == NULL) 
	{
		fprintf(stderr, "Oops! Ran out of memory!!! Exiting..");
		exit(1);	
	}
	elem->obj = data;
	
	elem->next = list->anchor.next;		
	list->anchor.next->prev = elem;
	elem->prev = &(list->anchor);
	list->anchor.next = elem;
	
	list->num_members++;		
	return TRUE;
}

int  ListInsertAfter(List* list, void* data, ListElem* elem)
{
	if(list == NULL) return FALSE;
	if(elem == NULL) return ListAppend(list, data);
	
	ListElem *newelem = (ListElem *) malloc (sizeof(ListElem));
	if(newelem == NULL) 
	{
		fprintf(stderr, "Oops! Ran out of memory!!! Exiting..");
		exit(1);	
	}
	newelem->obj = data;
	
	newelem->next = elem->next;		
	elem->next->prev = newelem;
	newelem->prev = elem;
	elem->next = newelem;
	
	list->num_members++;		
	return TRUE;
}

int  ListInsertBefore(List* list, void* data, ListElem* elem)
{
	if(list == NULL) return FALSE;
	if(elem == NULL) return ListPrepend(list, data);
	
	ListElem *newelem = (ListElem *) malloc (sizeof(ListElem));
	if(newelem == NULL) 
	{
		fprintf(stderr, "Oops! Ran out of memory!!! Exiting..");
		exit(1);	
	}
	newelem->obj = data;
	
	newelem->prev = elem->prev;		
	elem->prev->next = newelem;
	newelem->next = elem;
	elem->prev = newelem;
	
	list->num_members++;		
	return TRUE;
}

void ListUnlink(List* list, ListElem* elem)
{
	if(list == NULL || ListEmpty(list))
	{
		fprintf(stderr, "The list provided is null or empty!!!\n");
		return;
	}
	
	if(elem == NULL)
	{
		fprintf(stderr, "The element to be unlinked is null!!!\n");
		return;
	}
	
	
	ListElem *prev_elem = elem->prev;
	prev_elem->next = elem->next;
	elem->next->prev = prev_elem;
	elem->next = elem->prev = NULL;
	free(elem);
	
	/*
	if(elem != NULL) 
	{
		printf("Cannot free memory. Exiting..");
		exit(1);
	}
	*/
	list->num_members--;

}

void ListUnlinkAll(List* list)
{
	if(list == NULL || ListEmpty(list))
	{
		fprintf(stderr, "The list provided is null or empty!!!\n");
		return;
	}
	
	
	ListElem *elem = NULL, *temp = NULL;
	
	for(elem=ListFirst(list); elem!=NULL; )
	{
		temp = ListNext(list, elem);
		ListUnlink(list, elem);
		elem = temp;
	}
	
	if(!ListEmpty(list)) 
	{
		fprintf(stderr, "Error occurred while unlinking all nodes. Exiting..\n");
		exit(1);
	}

	return;

}

ListElem *ListFind(List* list, void* data)
{
	ListElem *elem = NULL;
	for(elem=ListFirst(list); elem!=NULL; elem = ListNext(list, elem))
	{
		if(elem->obj == data)
			return elem;
	}

	return NULL;	
}

int ListExistsById(List* list, long long  id)
{
	ListElem *elem = NULL;
	header_t *h = NULL;

	for(elem=ListFirst(list); elem!=NULL; elem = ListNext(list, elem))
	{
		h = (header_t *) &(((packet_t *) elem->obj)->msg_header);
		if(h->pkt_id == id)
			return 1;
	}

	return 0;	
}

int InsertIntoList(List *list, void *element)
{
        ListElem *fst = NULL, *lst = NULL;

        //Initialize the list if it is empty
        if(ListEmpty(list))
        {
		/*
                if(!ListInit(list))
                {
                        fprintf(stderr, "The list could not be initialized. Exiting..");
                        exit(1);
                }
		*/
                return ListAppend(list, (void *) element);
        }

        for(fst=ListFirst(list), lst=ListLast(list); CompareTransElem(fst->obj, lst->obj) <= 0; fst=ListNext
(list, fst), lst=ListPrev(list, lst))
        {
                if((CompareTransElem(element, fst->obj) == 0) || (CompareTransElem(element, lst->obj) == 0))
                {
                        fprintf(stderr, "Match Found: Should not have happenned!!! Exiting..\n");
                        exit(1);
                }

                if(CompareTransElem(element, fst->obj) < 0)
                {
                        return ListInsertBefore(list, (void *) element, fst);
                }
		
		if((CompareTransElem(element, lst->obj) > 0))
                {
                        return ListInsertAfter(list, (void *) element, lst);
                }
        }

        if(CompareTransElem(element, fst->obj) < 0)
        {
                return ListInsertBefore(list, (void *) element, fst);
        }

        if((CompareTransElem(element, lst->obj) > 0))
        {
                return ListInsertAfter(list, (void *) element, lst);
        }

        return FALSE;
}

int CompareTransElem(void *src, void *dst)
{
	packet_t *p_src = (packet_t *) src, *p_dst = (packet_t *) dst;
	header_t *h_src = (header_t *) &p_src->msg_header, *h_dst = (header_t *) &p_dst->msg_header;

        if(h_src == NULL || h_dst == NULL)
                return -1;
        else if (h_src->pkt_id == h_dst->pkt_id)
                return 0;
        else if(h_src->pkt_id < h_dst->pkt_id)
                return -1;
        else
                return 1;
}

